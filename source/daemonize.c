#include "dprc.h"
#include <fcntl.h>

int	daemonize(void)
{
	int maxfd, temp_fd;

	temp_fd = 0;
	if (getppid() == 1)
	{
		return (EXIT_FAIL);
	}

	switch (fork())
	{
		case -1:	return (EXIT_FAIL);
		case 0:		break;
		default:	exit(EXIT_SUCCESS);
	}
	if (setsid() == EXIT_FAIL)
		return (EXIT_FAIL);

	switch (fork())
	{
	case -1:
		return (EXIT_FAIL);
	case 0:
		break;
	default:
		exit(EXIT_SUCCESS);
	}
	umask(0);
	chdir("/");

	maxfd = sysconf(_SC_OPEN_MAX);
	if (maxfd == -1)
		maxfd = S_MAX_OPEN_FILE;
	while (temp_fd < maxfd)
		close(temp_fd++);
	return (EXIT_SUCCESS);
}
