#include "catenation.h"
#include "get_next_line.h"
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>

int		open_db_file(char *interface)
{
	int		fd;
	char	*temp_path;
	char	*path;

	if (!(temp_path = strjoin(DB_FILE, interface)))
		return (-1);
	if (!(path = strjoin(temp_path, DB_TYPE)))
	{
		free(path);
		return (-1);
	}
	fd = open(path, O_CREAT | O_WRONLY, 0666);
	free(temp_path);
	free(path);
	return (fd);
}

void	load_data(t_tree *ip_tree, const char *interface, int fd)
{
	char	*data;
	char 	*pack;
	int64_t ip;
	int64_t package;

	if (!ip_tree)
		return ;
	
	while (get_next_line(fd, &data) > 0)
	{
		if (!(strcmp(data, interface)))
		{
			free(data);
			while (get_next_line(fd, &data) > 0)
			{
					ip = atoll(data);
					if (ip < 0x19999999 && ip > 0)
					{
						if ((pack = strchr(data, ',')))
						{
							package = atoll(pack + 1);
							if (package > 0)
								push_ip_package(ip_tree, ip, package);
						}
					}
					free(data);
			}
			return ;
		}
		else
			free(data);
	}
}