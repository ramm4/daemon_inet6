#include <stdlib.h>
#include "b-tree.h"

static void	push(t_tree **leaf, int64_t ip)
{
	(*leaf) = create_b_tree();
	(*leaf)->ip = ip;
	(*leaf)->package_sent += 1;
}

t_tree		*find_ip(int64_t ip, t_tree *root)
{
	t_tree	*leaf;

	if (!root)
		return(NULL);

	leaf = root;

	while(leaf->ip != ip)
	{
		if (leaf->ip < ip)
		{
			if (leaf->right)
				leaf = leaf->right;
			else
				return (NULL);
		}
		else if (leaf->ip > ip)
		{
			if (leaf->left)
				leaf = leaf->left;
			else
				return (NULL);
		}
	}
	return (leaf);
}

t_tree		*create_b_tree()
{
	t_tree	*root;

	if (!(root = (t_tree*)malloc(sizeof(t_tree))))
		return (NULL);
	else
	{
		root->right = NULL;
		root->left = NULL; 
		root->ip = 0;
		root->package_sent = 0;
	}
	return (root);
}

t_tree		*bfree(t_tree *root)
{
	t_tree	*right_side;
	t_tree	*left_side;
	t_tree	*temp_root = NULL;
	t_tree	*node = root;

	while (node)
	{
		if (node->left)
		{
			left_side = node->left;
			node->left = temp_root;
			temp_root = node;
			node = left_side;
		}
		else if (node->right)
		{
			right_side = node->right;
			node->left = temp_root;
			node->right = NULL;
			temp_root = node;
			node = right_side;
		}
		else
		{
			if (!temp_root)
			{
				free(node);
				node = NULL;
			}
			while (temp_root)
			{
				free(node);
				if (temp_root->right)
				{
					node = temp_root->right;
					temp_root->right = NULL;
					break;
				}
				else
				{
					node = temp_root;
					temp_root = temp_root->left;
				}
			}
		}
	}
	return (NULL);
}

void		push_ip_package(t_tree *root, int64_t ip, int64_t package)
{
	t_tree	*leaf;

	if (!root || !(root->ip))
	{
		root->ip = ip;
		root->package_sent += package;
		return;
	}

	leaf = root;
	while(leaf->ip != ip)
	{
		if (leaf->ip < ip)
		{
			if (!leaf->right)
				return(push(&leaf->right, ip));
			else
				leaf = leaf->right;
		}
		else if (leaf->ip > ip)
		{
			if (!leaf->left)
				return (push(&leaf->left, ip));
			else
			leaf = leaf->left;
		}
		else if (!leaf)
			return (push(&leaf, ip));
	}

	if (leaf->ip == ip)
		leaf->package_sent += package;
}