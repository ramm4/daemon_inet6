#include "sniffer.h"
#include "catenation.h"
#include <string.h>
#include <stdio.h>
#include <netdb.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <sys/un.h>
#include <sys/select.h>
#include <netinet/in.h>
#include <errno.h>
#include "get_next_line.h"

static void print_logfile(int logfile, char *message, size_t len);
static int def_input_request(t_tree *root, t_cmmd_opt *opt, unsigned char *buffer, int log, int *mod);

int sniffer(t_tree *ip_tree,t_cmmd_opt *option, int *status)
{
	int					masterSocket, cli_socket, nw;
	int					logfile, logging_mode, fd_db;
	int					mod;
	ssize_t				data_size;

	struct sockaddr		clientaddr;
	struct ifreq		ifr;
	socklen_t			clientaddr_size;
	char				clienthost[NI_MAXHOST]; //The clienthost will hold the IP address.

	struct sockaddr_un  cli_server_addr;
	struct sockaddr_un	cli_client_addr;
	socklen_t			cli_addr_size;

	unsigned char		*buffer = (unsigned char *)malloc(BUFF_SIZE);

	fd_set				readfds;
	int					max_fd;
	int					activity;

	logging_mode = LOGFILE_MOD;
	logfile = -1;
	char hh[] = "yrdddd";
	mod = 1;
	if (logging_mode)
		logfile = open("daemon_sniffer_servise.log", O_CREAT | O_APPEND | O_WRONLY, 0666);
	memset(&clientaddr, 0, sizeof(struct sockaddr));
	memset(&ifr, 0, sizeof(ifr));
	memset(&cli_server_addr, 0, sizeof(struct sockaddr_un));
	memset(&cli_client_addr, 0, sizeof(struct sockaddr_un));
	snprintf(ifr.ifr_name, sizeof(ifr.ifr_name), "%s", option->interface);

	if (logfile < 0)
		logging_mode = 0;
	else
		print_logfile(logfile, "\tStarted sniffering\n", 20);

	if ((masterSocket = socket(AF_INET, SOCK_RAW | O_NONBLOCK, IPPROTO_TCP)) < 0)
	{
		if (logging_mode)
			print_logfile(logfile, "Failed to get packets\n", 23);
		return (EXIT_FAILURE);
	}
	ioctl(masterSocket, if_nametoindex(ifr.ifr_name), &ifr);
	if (setsockopt(masterSocket, SOL_SOCKET, SO_BINDTODEVICE, (void *)&ifr, sizeof(struct ifreq)) < 0)
	{
		if (logging_mode)
			print_logfile(logfile, "ERROR: SO_BINDTODEVICE\n", 23);
		close(masterSocket);
		return (EXIT_FAILURE);
	}
	else
	{
		if ((fd_db = open_db_file(option->interface)) < 0)
		{
			close(masterSocket);
			return (EXIT_FAILURE);
		}
		else
			load_data(ip_tree, option->interface, fd_db);
	}
	
	if ((cli_socket = socket(AF_UNIX, SOCK_STREAM | O_NONBLOCK, 0)) < 0)
	{
		if (logging_mode)
			print_logfile(logfile, "SOCKET ERROR:\n", 14);
		return (EXIT_FAILURE);
	}
	cli_server_addr.sun_family = AF_UNIX;
	strncpy(cli_server_addr.sun_path, SOCK_PATH, sizeof(cli_server_addr.sun_path) - 1);
	unlink(SOCK_PATH);
	if ((bind(cli_socket, (struct sockaddr *)&cli_server_addr, sizeof(cli_server_addr))) < 0)
	{
		print_logfile(logfile, "bind error\n", 11);
		return (EXIT_FAILURE);
	}

	listen(masterSocket, SRV_BACKLOG);
	listen(cli_socket, SRV_BACKLOG);

	while (mod)
	{
	  	FD_ZERO(&readfds);
		FD_SET(masterSocket, &readfds);
		FD_SET(cli_socket, &readfds);

		if (cli_socket > masterSocket)
			max_fd = cli_socket;
		else
			max_fd = masterSocket;

		activity = select( max_fd + 1 , &readfds , NULL , NULL , NULL);

	   if ((activity < 0) && (errno!=EINTR)) 
			print_logfile(logfile, "Select error\n", 13);
	   else
	   {	   
	   		if (FD_ISSET(masterSocket, &readfds))
	    	{
	       		if ((data_size = recvfrom(masterSocket, buffer, BUFF_SIZE, 0,  &clientaddr, &clientaddr_size)) < 0)
				{
				   if (logging_mode)
						print_logfile(logfile, "Recvfrom error , failed to get packets\n", 39);
				}
	       		else
		 		{
					memset(clienthost, 0, NI_MAXHOST);
	 				getnameinfo(&clientaddr, clientaddr_size, clienthost, sizeof(clienthost), NULL,
			    		0, NI_NUMERICHOST | NI_NUMERICSERV);
					push_ip_package(ip_tree, inet_addr(clienthost), 1);
					if (logging_mode)
					{
						
						print_logfile(logfile, "***IP***\n", 9);
						print_logfile(logfile, clienthost, strlen(clienthost));
						write(logfile, "\n", 1);
					}
		 		}
	   		}
			if (FD_ISSET(cli_socket, &readfds))
	     	{
				if ((nw = accept(cli_socket, (struct sockaddr *)&cli_client_addr, (socklen_t *)&cli_addr_size)) < 0)
				{
					print_logfile(logfile, "ACCEPT ERROR\n", 15);
				}
				else if ((data_size = recv(nw, buffer, BUFF_SIZE, 0)) < 0)
				{
					if (logging_mode)
						print_logfile(logfile, "DATA RECV FAIL\n", 15);
				}
	       		else
				{
					if (!(def_input_request(ip_tree, option, buffer, logfile, &mod)))
					{
					  if (send(nw, buffer, strlen((char *)buffer), 0) < 0)
						{
							print_logfile(logfile, "SEND ERROR\n", 12);
						}
					}
		 		}
	  		}
		}
	}
	close(masterSocket);
	close(cli_socket);
	free(buffer);

	if (logging_mode)
		write(logfile, "Finished\n", 9);
	return (EXIT_SUCCESS);
}

static int	def_input_request(t_tree *root, t_cmmd_opt *opt, unsigned char *buffer, int log, int *command)
{
	t_tree	*leaf;
	char	*temp_ip_buff;

	descript_transmission(opt, buffer);

	if (opt->mod == SHOW_PACKAGE_FOR_IP_MOD)
	{
		if(!(leaf = find_ip(opt->ip, root)))
			strcpy((char *)buffer, "NOT FOUND");
		else
		{
			struct sockaddr_in k;

			memset(&k, 0, sizeof(struct sockaddr_in));
			k.sin_addr.s_addr = leaf->ip;
			k.sin_family = AF_INET;
			temp_ip_buff = (char *)malloc(INET_ADDRSTRLEN);
			inet_ntop(AF_INET, &k.sin_addr, temp_ip_buff, INET_ADDRSTRLEN);
			snprintf((char *)buffer, BUFF_SIZE, "%s receved %lli packages\n", temp_ip_buff, leaf->package_sent);
			free(temp_ip_buff);
		}
	}
	else if (opt->mod == CHANGE_INTERFACE)
	{
		strcpy((char *)buffer, "Daemon is going to restart and change Enternet interface");
		*command = RESTART_;	
	}
	return (EXIT_SUCCESS);
}

static void	print_logfile(int logfile, char *message, size_t len)
{
	write(logfile, message, len);
}
