#include "dprc.h"
#include "catenation.h"
#include <syslog.h>

int	main(void)
{
	char		*temp_interface = "enp3s0:";
	t_tree		*ip_tree =				create_b_tree();
	int			status;
	t_cmmd_opt	option;
	//	int		fd_db;

	(void)ip_tree;
	if (daemonize() < 0)
		return (EXIT_FAILURE);
	//		syslog(LOG_NOTICE, "First daemon started.");
	//fd_db = open_db_file();
	//load_data(ip_tree, temp_interface, fd_db);

	option.interface = strdup(temp_interface);
	status = 1;
	while(status)
	{
		if (sniffer(ip_tree, &option, &status))
			return (EXIT_FAILURE);
		//		syslog(LOG_NOTICE, "First daemon terminated.");
	}
	free(option.interface);
	closelog();
	return (EXIT_SUCCESS);
}
