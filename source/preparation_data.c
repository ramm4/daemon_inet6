#include "sniffer.h"

static unsigned char *serialize_ip(t_cmmd_opt *option, unsigned char *buffer)
{
	buffer[0] = (option->ip >> 56) & 0xFF;
	buffer[1] = (option->ip >> 48) & 0xFF;
	buffer[2] = (option->ip >> 40) & 0xFF ;
	buffer[3] = (option->ip >> 32) & 0xFF;
	buffer[4] = (option->ip >> 24) & 0xFF;
	buffer[5] = (option->ip >> 16) & 0xFF;
	buffer[6] = (option->ip >> 8) & 0xFF;
	buffer[7] = (option->ip) & 0xFF;
	return (buffer + 8);
}

static unsigned char *serialize_mod(t_cmmd_opt *option, unsigned char *buffer)
{
	buffer[0] = (option->mod) & 0xFF;
	buffer[1] = (option->mod >> 8) & 0xFF;
	buffer[2] = (option->mod >> 16) & 0xFF;
	buffer[3] = (option->mod >> 24) & 0xFF;
	return (buffer + 4);
}

unsigned char		*pack_struct_option_to_receive(t_cmmd_opt *option, size_t *size)
{
	unsigned char	*data;
	unsigned char	*ip;
	unsigned char	*mod;
	size_t			len;

	len = sizeof(option->mod) + sizeof(option->ip) +
		  (strlen(option->interface) + 1);
	if (!(data = (unsigned char *)malloc(len)))
		return (NULL);
	memset(data, 0, len);
	mod = serialize_mod(option, data);
	ip = serialize_ip(option, mod);
	strcpy(ip, option->interface);
	*size = len;
	return (data);
}

void				descript_transmission(t_cmmd_opt *opt, unsigned char *s_data)
{
	size_t ip_size = sizeof(int64_t);
    unsigned char	*data;

	opt->mod = 0;
	opt->mod |= (s_data[3] << 24);
	opt->mod |= (s_data[2] << 16);
	opt->mod |= (s_data[1] << 8);
	opt->mod |= s_data[0];

	data = s_data + 4;
	opt->ip = 0ULL;
	opt->ip = (opt->ip) | data[0];
	opt->ip = (opt->ip << 8) | data[1];
	opt->ip = (opt->ip << 8) | data[2];
	opt->ip = (opt->ip << 8) | data[3];
	opt->ip = (opt->ip << 8) | data[4];
	opt->ip = (opt->ip << 8) | data[5];
	opt->ip = (opt->ip << 8) | data[6];
	opt->ip = (opt->ip << 8) | data[7];

	if (opt->mod == CHANGE_INTERFACE)
	{
		free(opt->interface);
		opt->interface = strdup(data + 8);
	}
}
