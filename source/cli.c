#include <stdlib.h>
#include <sys/un.h>
#include <netdb.h>
#include <string.h>
#include <unistd.h>
#include "sniffer.h"
#include <arpa/inet.h>

int show_mode(int argc, int cmmnd_argc, char **argv, t_cmmd_opt *option)
{
  //  struct sockaddr_in ip;
  
	if (argc != cmmnd_argc + 2)
		return (EXIT_FAILURE);

	option->ip = inet_addr(argv[2]);
	if (option->ip < 0)
	  {
	    //	    printf("%s  %dl  Error result\n", argv[2], ip.sin_addr.s_addr);
	    return (EXIT_FAILURE);
	  }
	if (strcmp(argv[3], "count"))
	  {
	    printf("ARGV3\n");
		return (EXIT_FAILURE);
	  }
	option->mod = SHOW_PACKAGE_FOR_IP_MOD;
	return (EXIT_SUCCESS);
}

int select_mode(int argc, int cmmnd_argc, char **argv, t_cmmd_opt *option)
{
	if (argc != cmmnd_argc + 2)
		return (EXIT_FAILURE);

	if (strcmp(argv[2], "iface"))
	{
		printf("error: undefined argument\n");
		return (EXIT_FAILURE);
	}
	option->mod = CHANGE_INTERFACE;
	option->interface = argv[3];
	return (EXIT_SUCCESS);
}

static t_cmmd init_cmmd[] = { {"show", 2, &show_mode}, {"select", 2, &select_mode}};

static void show_usage(void);

static int send_command(t_cmmd_opt *option)
{
	struct sockaddr_un	srv_addr;
	struct sockaddr_un	client_sockaddr;
	int					socket_fd;
	char				*buffer = malloc(BUFF_SIZE);
	unsigned char				*data_to_send;
	size_t						data_to_send_size;

	if ((socket_fd = socket(AF_UNIX, SOCK_STREAM, 0)) < 0)
	{
		printf("socket error");
		return (EXIT_FAILURE);
	}

	memset(&client_sockaddr, 0, sizeof(struct sockaddr_un));
	client_sockaddr.sun_family = AF_UNIX;
	strcpy(client_sockaddr.sun_path, CLIENT_PATH);
	unlink(CLIENT_PATH);
	if (bind(socket_fd, (struct sockaddr *)&client_sockaddr, sizeof(struct sockaddr_un)) < 0)
	{
		printf("BIND ERROR:\n");
		close(socket_fd);
		exit(1);
	}

	memset(&srv_addr, 0, sizeof(struct sockaddr_un));
	srv_addr.sun_family = AF_UNIX;
	strncpy(srv_addr.sun_path, SOCK_PATH, sizeof(srv_addr.sun_path) - 1);
	if (connect(socket_fd, (struct sockaddr *)&srv_addr, sizeof(struct sockaddr_un)) < 0)
	{
		printf("CONNECT ERROR\n");
		close(socket_fd);
		exit(1);
	}
	if ((data_to_send = pack_struct_option_to_receive(option, &data_to_send_size)))
	{
		printf("%lld\n", option->ip);
		printf("%d %d %d %d\n", data_to_send[0], data_to_send[1], data_to_send[2], data_to_send[3]);
		if (send(socket_fd, data_to_send, data_to_send_size, 0) < 0)
		{
			printf("SEND ERROR\n");
			close(socket_fd);
			exit(1);
		}
		if(recv(socket_fd, buffer, BUFF_SIZE, 0) < 0)
		{
			printf("RECV ERROR\n");
			close(socket_fd);
			exit(1);
		}
	}
		printf("%s\n", buffer);
		return (EXIT_SUCCESS);
}

static int def_command(int argc, char **argv, t_cmmd_opt *option)
{
	int	i;

	i = 0;
	while (init_cmmd[i].cmmd)
	{
		if (!strcmp(init_cmmd[i].cmmd, argv[1]))
		{
			if ((init_cmmd[i].fun_ptr(argc, init_cmmd[i].argc, argv, option)))
			    return (EXIT_FAILURE);
			else
			  return (EXIT_SUCCESS);
		}
		++i;
	}
	return (EXIT_FAILURE);
}

int	main(int argc, char **argv)
{
	t_cmmd_opt	option;

	if (argc >= 0x2 && argc <= 0x4)
	{
		if (!def_command(argc, argv, &option))
		  {
		    /*TEST*/printf("****%d\n", option.mod);
			send_command(&option);
		  }
	}
	else
		show_usage();

	return (EXIT_SUCCESS);
}

static void show_usage(void)
{
	printf("------------------------------------------------------------------------\n");
	printf("%-22s %-48s", "|start", "sniffing from default iface(eth0)");
	printf("|\n------------------------------------------------------------------------\n");
	printf("%-22s %-48s", "|stop", "packets are not sniffed");
	printf("|\n------------------------------------------------------------------------\n");
	printf("%-22s %-48s", "|show [ip] count", "print number of packets received from ip address");
	printf("|\n------------------------------------------------------------------------\n");
	printf("%-22s %-48s", "|select iface [iface]", "interface for sniffing eth0, wlan0, ethN, etc");
	printf("|\n------------------------------------------------------------------------\n");
	printf("%-22s %-48s", "|sstat   [iface]", "show all collected statistics for interface");
	printf("|\n------------------------------------------------------------------------\n");
	printf("%-22s %-48s", "|--help", "show usage information");
	printf("|\n------------------------------------------------------------------------\n");
}
