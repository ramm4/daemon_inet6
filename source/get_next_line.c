/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lmatvien <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/08 16:49:37 by lmatvien          #+#    #+#             */
/*   Updated: 2018/05/25 13:47:59 by lmatvien         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

char *strjoin(char const *s1, char const *s2)
{
	const char *ptr_s;
	size_t size;

	if (!s1 && !s2)
		return (NULL);
	size = strlen(s1) + strlen(s2) + 1;
	ptr_s = malloc(size);
	if (!ptr_s)
		return (NULL);
	if (s1 != NULL)
		strcpy((char *)ptr_s, (char *)s1);
	if (s2 != NULL)
		strcat((char *)ptr_s, s2);
	return ((char *)ptr_s);
}

static int	get_line(char **des, char **line)
{
	size_t	end_l;
	char	*temp;

	if (!(*des) || **des == '\0')
		return (-1);
	if (**des == '\n')
	{
		*line = strcpy(malloc(1), "\0");
		temp = strcpy(malloc(_BUFF_SIZE_ - 1), *des + 1);
		free(*des);
		*des = temp;
		return (1);
	}
	if ((end_l = strnlen(*des, '\n')))
	{
		*line = strncat(malloc(end_l), *des, end_l);
		temp = strcpy(malloc(strlen(*des) - end_l), *des + end_l + 1);
		free(*des);
		*des = temp;
		return (1);
	}
	return (0);
}

static int	status_line(char **des, char **line, int *status, int *bytes)
{
	if (*status == 1)
		return (1);
	if (*bytes < 0)
		return (-1);
	if ((*status = get_line(des, line)) == 0)
	{
		*line = *des;
		*des = NULL;
		return (1);
	}
	else if (*status > 0)
		return (1);
	else
		*line = NULL;
	return (0);
}

int	get_next_line(const int fd, char **line)
{
	int				status;
	int				bytes;
	static char		*des[256];
	char			*buffer;
	char			*temp;

	if (fd < 0 || fd > 256 || _BUFF_SIZE_ <= 0 || !line)
		return (-1);
	if ((status = get_line(&des[fd], line)) == 1)
		return (1);
	if (!(buffer = malloc(_BUFF_SIZE_)))
		return (-1);
	while ((bytes = read(fd, buffer, _BUFF_SIZE_)) > 0)
	{
		buffer[bytes] = '\0';
		temp = strjoin(des[fd], buffer);
		free(des[fd]);
		des[fd] = temp;
		if ((status = get_line(&des[fd], line)) == 1)
			break ;
	}
	free(buffer);
	return (status_line(&des[fd], &(*line), &status, &bytes));
}
