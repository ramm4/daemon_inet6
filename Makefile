TARGER = chrome
TARGER2 = cli-chrome
FLAGS = 

HEADER = ./include

SOURCE = ./source

PRJ__FILES =	$(SOURCE)/main.c \
				$(SOURCE)/daemonize.c \
				$(SOURCE)/sniffer.c \
				$(SOURCE)/preparation_data.c \
				$(SOURCE)/b-tree.c \
				$(SOURCE)/get_next_line.c \
				$(SOURCE)/db_control.c \

CLI__FILES = 	$(SOURCE)/cli.c \
				$(SOURCE)/preparation_data.c \


all:
	gcc -o $(TARGER) $(PRJ__FILES) -I $(HEADER) $(FLAGS)
	gcc -o $(TARGER2) $(CLI__FILES) $(FLAGS) -I $(HEADER)

clean:
	/bin/rm -f $(TARGER)
	/bin/rm -f $(TARGER2)

rebuild: clean all
