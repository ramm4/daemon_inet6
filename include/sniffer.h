#ifndef 	_SNIFFER_H_
# define	_SNIFFER_H_

#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>

#ifndef SRV_BACKLOG
# define SRV_BACKLOG 10
#endif

#ifndef SO_BINDTODEVICE
# define SO_BINDTODEVICE INADDR_ANY
#endif

#define BUFF_SIZE	65536
#define LOGFILE_MOD	1
#define SOCK_PATH	"/tmp/daemon_sniffer_socket_serv"
#define CLIENT_PATH "/tmp/daemon_sniffer_socket_client"

#define SHOW_PACKAGE_FOR_IP_MOD	0x201
#define CHANGE_INTERFACE		0x111
#define RESTART_				0x0

typedef struct		cmmnd_opt
{
	char			*interface;
	int32_t			mod;
	int64_t			ip;
}					t_cmmd_opt;

typedef struct		initial_cmmd
{
	char			*cmmd;
	int				argc;
	int				(*fun_ptr)(int, int, char **, t_cmmd_opt *);
}					t_cmmd;

unsigned char		*pack_struct_option_to_receive(t_cmmd_opt *option, size_t *len);
void				descript_transmission(t_cmmd_opt *opt, unsigned char *data);

#endif
