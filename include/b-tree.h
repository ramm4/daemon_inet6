#ifndef _B_TREE_H_
#define _B_TREE_H_

#include <stdint.h>

typedef struct	tree
{
	struct tree *right;
	struct tree *left;
	int64_t		ip;
	int64_t 	package_sent;
}				t_tree;

t_tree			*create_b_tree();
t_tree			*bfree(t_tree *root);
void			push_ip_package(t_tree *root, int64_t ip, int64_t package);
t_tree			*find_ip(int64_t ip, t_tree *root);

#endif