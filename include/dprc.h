#ifndef _DPRC_H_
# define _DPRC_H_

#include <unistd.h>
#include <stdlib.h>
#include <sys/stat.h>

#define EXIT_FAIL		-1
#define S_MAX_OPEN_FILE 8192

int		daemonize(void);

#endif