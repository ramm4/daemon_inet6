#ifndef _CATENATION_H_
#define _CATENATION_H_

#include "b-tree.h"
#include "sniffer.h"

#define DB_FILE "/var/"
#define DB_TYPE ".db"

int		sniffer(t_tree *ip_tree, t_cmmd_opt *option, int *status);
int		open_db_file(char *interface);
void	load_data(t_tree *ip_tree, const char *interface, int fd);

#endif